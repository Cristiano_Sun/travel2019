.. travel2019 documentation master file, created by
   sphinx-quickstart on Fri Oct 19 14:09:14 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

瑞士之行
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   geneva
   interlaken
   spiez
   luzern
   montreux
   zermatt
