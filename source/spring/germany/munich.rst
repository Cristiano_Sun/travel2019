慕尼黑
======

一、主要景点
------------

1. **新天鹅堡**
~~~~~~~~~~~~~~~

-  介绍 ::

  新天鹅堡是德国名气最响，旅行者最多的城堡。这座路德维希二世倾尽国力的杰作，拥有童话般的容颜与风景。当你站在玛利亚桥上眺望，无法不为这一切倾倒。新天鹅堡的位置在小镇菲森的附近。如果你想在这周围停留过夜，菲森镇是一个不错的选择。这也是一个可以追溯历史至中世纪的古镇了。

-  信息

   -  路线：慕尼黑的中央火车站出发，乘开往菲森的火车。车票可以使用火车通票和拜仁票，拜仁票非周末或法定节日必须在上午9点以后使用。
   -  门票：9欧（包括讲解）

2. 因戈尔施塔特(Ingolstadt)奥特莱斯
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  介绍 ::

  因戈尔施塔特打折村 (Ingolstadt Village，`查看相关信息<www.ingolstadtvillage.com>`_)和 韦尔泰姆打折村 (Wertheim Village，`查看相关信息<www.wertheimvillage.com>`_)是另两个比较著名的打折村，同属Chic Outlet Shopping集团。这里有少量一线品牌，更多的是德国本土品牌和休闲品牌。除非你对购物很有兴趣，不然并不值得专程前往。交通方式也以自驾最为方便。Ingolstadt有大巴往返于慕尼黑，Wertheim有大巴往返于法兰克福，关注这两个打折村的微博，可以获得最新的车票优惠码信息。打折村的详细品牌列表和分布，可以在各自的主页上查询。另外，还可以在微博上找到不少信息，除了德国国家旅游局以外，一些著名的商家也有推广账号，如柏林KaDeWe卡迪威百货、慕尼黑Oberpollinger百货 、汉堡Alsterhaus百货 、麦琴根名品村、德国Ingolstadt购物村、德国Wertheim购物村等等，还经常通过微博推广促销活动哦。

-  信息

   -  营业时间：绝大部分的商店都是周一至周六，最晚到20:00，有些可能只到19:00或者周六更早关门。巴伐利亚州的商店和超市最为保守，20:00一定关门，其他州的超市会有开到22:00甚至24:00，周日也有可能开门。火车站内的店和超市周日是营业的。旅游纪念品店在周日和节假日也基本是营业的。
   -  打折季
      ：冬季打折季(WSV，Winterschlussverkauf)，通常始于圣诞假期后的第1天，即12月27日，持续到1月底左右。夏季打折季(SSV，Sommerschlussverkauf)，通常始于6月的后两周，持续到7月底左右。季中打折(Mid-Season
      Sale)很少，主要是一些流行服饰品牌。

二、退税
--------

-  在德国的退税主要通过环球蓝联(Global
   Blue)退税公司操作，也有少数是通过PremierTaxFree公司。他们的标志都是蓝颜色为主的，标志上都有“Tax
   Free”的字样，贴在提供服务的商店门口。允许退税的购物最低金额为 **25欧元**，退税的比例根据商品金额的高低也有不同，在
   **8%** 至 **14.5%**
   之间，金额越高退税比例越高。在购物时，告知收银员需要“Tax
   Free”，会收到一张表格，或是随账单一起打印出来的一张很长的纸条，上面已有了应退的金额，下面有许多需要填写的内容。

   -  请在最后到达机场前，用大写英文字母认真填写，内容包括：

      -  护照号码(Passport No.)；
      -  名字及姓氏(Name)；
      -  地址(Address)；
      -  邮编和城市(ZIP / City)；
      -  国家(Country)；
      -  电子邮箱(Email Address)。
      -  如需将税金退还到信用卡内，需填写信用卡号(Credit Card
         No.)；还可将税金退到支付宝(Alipay)账户内，填写与支付宝账号绑定的中国手机号码即可；
      -  最后签名(Signature)。

-  基本上所有商店现在都是直接在收银台要求退税单的，Kaufhof百货商场除外，他们的退税单是在商场的Service
   Point取的，而且同一商场可以三个月内的账单一起开一张退税单。如果是比较贵重的商品，在开退税单时，可以要求一张账单的复印件，原始账单在退税时会被收走。机场退税退税的商品分两类，一类是在托运行李中，一类是在随身行李中，请将两类退税单分开整理。托运行李中如有退税物品，请在值机(Check-in)时申明，则行李将被取回，带到海关供检查，有时会被要求开箱核对商品，海关确认后会在退税单上敲章。凭敲章的退税单前往环球蓝联的柜台或者是为Premier提供服务的Reisebank柜台领取现金。
-  德国已支持支付宝退税。在有环球蓝联(Global
   Blue)标识的商户购物后，于退税单找到Alipay一栏，填写与支付宝账户绑定的中国手机号码，离境后的10-15个工作日内，税金会直接以人民币形式退到支付宝账户中。支付宝退税比信用卡更快，到帐后会收到消息通知，并可在支付宝账单中查询到此笔退税信息。

   -  如果选择现金退税，环球蓝联将收取每张退税单3欧元的手续费。选择退税至信用卡或支付宝则无需手续费。如果选择退还到信用卡里的，可以根据退税表上的ID号，在用环球蓝联网站上的退税追踪器(Refund
      Tracker)来了解退款的处理状态。
   -  另外，一次旅行中，欧盟国家购买的商品是统一在最后一个离境的国家退税的，因此如果从德国进出，其他国家买的东西也能退税。但是如果是欧盟国家的转机航班，比如法航或者荷兰皇家航空，就更复杂一点。托运行李还是在德国机场退，但随身行李就要到转机机场出了海关以后再退了。
